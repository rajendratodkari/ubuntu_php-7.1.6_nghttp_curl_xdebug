FROM ubuntu:latest
MAINTAINER Rajendra Todkari <rajendra.todkari@gmail.com>

ENV PHP_VERSION 7.1.6
ENV OPENSSL_VERSION 1.1.0f
ENV PHP_INSTALL_DIR /usr/local/php7
ENV CURL_VERSION 7.54.0
ENV XDEBUG_VERSION 2.5.4

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y dist-upgrade

#install default libs
RUN apt-get install -y --fix-missing \
    build-essential \
	apt-utils

RUN apt-get install -y \
    cmake \
    git-core \
    autoconf \
    automake \
    autotools-dev \
    libtool \
	libcunit1-dev \
    bison \
    libev-dev \
    libevent-dev \
    libjansson-dev \
    libxml2 \
    libbz2-dev \
    libmcrypt-dev \
    pkg-config \
    libltdl-dev \
    libjpeg-dev \
    libpng-dev \
    libxpm-dev \
    libfreetype6-dev \
    libpng12-dev \
    libpspell-dev \
    libreadline-dev \
    libicu-dev \
    libxml2-dev \
    libpng-dev \
    libfreetype6 \
    libfreetype6-dev \
    imagemagick \
    libmagickwand-dev \
    zlib1g-dev \
	ca-certificates \
	git \
	libgmp-dev \
	mcrypt \
	perl \
	g++ \
	libpspell-dev \
	librecode-dev \
	re2c \
	wget \
	make \
	psl \
    libpsl-dev \
	ant \
	openssh-server \
	supervisor \
	zip \
	idn \
	idn2 \
	binutils \
	libc-ares-dev \
	libjemalloc-dev \
	libsystemd-dev \
	libspdylay-dev \
	cython \
	python3-dev \
	python-setuptools

# Make ssh dir
RUN mkdir /root/.ssh/
# Copy over private key, and set permissions
ADD id_rsa /root/.ssh/id_rsa
RUN chmod 400 /root/.ssh/id_rsa
# Create known_hosts
RUN touch /root/.ssh/known_hosts
# Add bitbuckets key
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts


#Install openssl
RUN cd ~ && \
	wget https://www.openssl.org/source/openssl-$OPENSSL_VERSION.tar.gz
RUN cd ~ && \
	tar -zxvf openssl-$OPENSSL_VERSION.tar.gz && \
	cd openssl-$OPENSSL_VERSION && \
	./config --prefix=/usr/ --openssldir=/etc/ssl shared && \
	make && \
	make install

#Install nghttp2
RUN cd ~ && \
	git clone https://github.com/nghttp2/nghttp2.git
RUN cd ~ && \
	cd nghttp2 && \
	autoreconf -i && \
	automake && \
	autoconf && \
	./configure && \
	make && \
	make install

#Install CURL
# Important to run command 'ldconfig'
RUN cd ~ && \
	wget https://curl.haxx.se/download/curl-$CURL_VERSION.tar.gz
RUN cd ~ && \
	tar -xvf curl-$CURL_VERSION.tar.gz && \
	cd curl-$CURL_VERSION && \
	./configure --with-nghttp2=/usr/local --with-ssl && \
	make && \
	make install && \
	ldconfig

# RUN cp /usr/lib/x86_64-linux-gnu/libcurl.so.4 /usr/lib/x86_64-linux-gnu/libcurl.so.4_backup && cp /root/curl-7.54.0/lib/.libs/libcurl.so.4 /usr/lib/x86_64-linux-gnu


# Install php
	RUN cd ~ && \
		git clone https://git.php.net/repository/php-src.git
	RUN cd ~ && \
		cd php-src && \
		git fetch --all && \
		git checkout -b PHP-$PHP_VERSION origin/PHP-$PHP_VERSION && \
		./buildconf --force && \
		./configure \
		--prefix=$PHP_INSTALL_DIR \
		--with-config-file-path=$PHP_INSTALL_DIR/etc \
		--with-config-file-scan-dir=$PHP_INSTALL_DIR/etc/conf.d \
		--enable-mbstring \
		--enable-zip \
		--enable-bcmath \
		--enable-pcntl \
		--enable-ftp \
		--enable-exif \
		--enable-calendar \
		--enable-sysvmsg \
		--enable-sysvsem \
		--enable-sysvshm \
		--enable-wddx \
		--with-curl \
		--with-mcrypt \
		--with-iconv \
		--with-gmp \
		--with-pspell \
		--with-gd \
		--enable-gd-native-ttf \
		--enable-gd-jis-conv \
		--with-xpm-dir \
		--with-openssl \
		--with-mysql \
		--with-pdo-mysql \
		--with-gettext \
		--with-recode \
		--with-bz2 \
		--enable-filter \
		--enable-fpm \
		--with-freetype-dir \
		--with-jpeg-dir \
		--with-png-dir \
		--enable-intl \
		--enable-mysqlnd \
		--with-mysqli \
		--with-mysql-sock=/var/run/mysqld/mysqld.sock \
		--with-pdo-mysql=mysqlnd \
# #	--with-pdo-sqlite \
# #	--with-sqlite3 \
		--enable-opcache \
		--enable-simplexml \
		--enable-xmlreader \
		--enable-xmlwriter \
		--with-zlib \
		&& make && \
		make install

RUN cd ~/php-src/ && \
	cp -v php.ini-production $PHP_INSTALL_DIR/lib/php.ini && \
	mkdir $PHP_INSTALL_DIR/etc/conf.d && \
	echo -e "# Zend OPcache\n zend_extension=opcache.so" > $PHP_INSTALL_DIR/etc/conf.d/modules.ini && \
	mv -v $PHP_INSTALL_DIR/etc/php-fpm.d/www.conf.default $PHP_INSTALL_DIR/etc/php-fpm.d/www.conf && \
	mv -v $PHP_INSTALL_DIR/etc/php-fpm.conf.default $PHP_INSTALL_DIR/etc/php-fpm.conf && \
	sed 's/nobody/root/g' $PHP_INSTALL_DIR/etc/php-fpm.conf


RUN ln -s $PHP_INSTALL_DIR/bin/php /usr/bin/php
RUN ln -s $PHP_INSTALL_DIR/bin/phpize /usr/bin/phpize
RUN ln -s $PHP_INSTALL_DIR/bin/php-config /usr/bin/php-config

#Install composer
RUN cd ~ && \
    php -r "copy('http://getcomposer.org/installer', 'composer-setup.php');" && \
	# verification of hash can not be done the folllwing way as hash value changes whenever composer get updated
    #php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php
    # php -r "unlink('composer-setup.php');" && \
    # rm -r curl-$CURL_VERSION && \
    # rm -r /var/cache/apk && \
    # rm -r /usr/share/man && \
    # apk del curldeps


#Install xdebug
RUN cd ~ && \
    wget http://xdebug.org/files/xdebug-$XDEBUG_VERSION.tgz
RUN cd ~ && \
    tar -zxvf xdebug-$XDEBUG_VERSION.tgz && \
    rm xdebug-$XDEBUG_VERSION.tgz && \
    cd xdebug-$XDEBUG_VERSION/ && \
    phpize && \
    ./configure --enable-xdebug && \
    make && \
    make install && \
	mkdir -p $PHP_INSTALL_DIR/etc/conf.d/ && \
	echo "" > $PHP_INSTALL_DIR/etc/conf.d/xdebug.ini && \
    HOST_IP="$(/sbin/ip route|awk '/default/ { print $3 }')" && \
    sed -i "$ a\xdebug.remote_host=${HOST_IP}" $PHP_INSTALL_DIR/etc/conf.d/xdebug.ini && \
    sed -i "$ a\zend_extension=xdebug.so" $PHP_INSTALL_DIR/etc/conf.d/xdebug.ini

# #Install ios push-notification php lib
RUN cd ~ && \
	git clone https://github.com/edamov/pushok.git && \
	cd pushok && \
	cp -v ~/composer.phar ~/pushok/ && \
	php composer.phar self-update && \
	php composer.phar clear-cache && \
	php composer.phar require edamov/pushok

RUN apt-get install -y php7.0-gmp vim php-curl



RUN DEBIAN_FRONTEND='noninteractive' apt-get install -y --no-install-recommends apache2 libapache2-mod-php
EXPOSE 80

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2

RUN mkdir -p $APACHE_RUN_DIR $APACHE_LOCK_DIR $APACHE_LOG_DIR

# Install push notification script
RUN cd ~ && \
	git clone https://rajtodkari:my5neh%40l@bitbucket.org/rajendratodkari/ios_push.git && \
	#copy push notification script and p8 file
	cp ios_push/* ~/pushok/

RUN cp -R ~/pushok /var/www/html/


# Do not use document root other than /var/www/html/
# ENV APACHE_DOCUMENT_ROOT /root/pushok
# RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
# RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf


# RUN /bin/bash -c "source /etc/apache2/envvars"
# RUN gedit /etc/apache2/sites-available/000-default.conf
# RUN /usr/sbin/apache2 -V
 CMD ["/usr/sbin/apache2", "-D", "FOREGROUND"]



# ENTRYPOINT [ "/usr/sbin/apache2" ]
# CMD ["-D", "FOREGROUND"]
